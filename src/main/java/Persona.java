
public class Persona {
    //Atributos
    private int identificación;
    private String nombre;
    private String apellido;
    
    //Constructor
    public Persona(int identificación, String nombre, String apellido) throws IllegalArgumentException {
        //Manejo de Excepciones
        if(identificación <= 0) {
            throw new IllegalArgumentException("El número de identificación es incorrecto");
        }
        if(nombre == null || "".equals(nombre.trim())) {
            throw new IllegalArgumentException("El nombre no fue ingresado o no es correcto");
        }
        if(apellido == null || "".equals(apellido.trim())) {
            throw new IllegalArgumentException("El apellido no fue ingresado o no es correcto");
        }
        
        this.identificación = identificación;
        this.nombre = nombre;
        this.apellido = apellido;
    }
    
    //Métodos de acceso
    public int getIdentificación() {
        return identificación;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
    
}
