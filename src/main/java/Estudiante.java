
import java.util.LinkedList;
import java.util.List;


public class Estudiante extends Persona {
    //Atributos propios
    private int codigo;
    //Relaciones de asociacion uno a uno
    private Programa programa;
    //Relaciones de asociacion uno a muchos
    private List<Deuda>listadoDeudas;
    
    //Constructor
    public Estudiante(int codigo, Programa programa, int identificación, String nombre, String apellido) throws IllegalArgumentException {
        //Hago el llamado al constructor de la super clase,
        //con el objetivo de inicializar los atributos que se heredan
        super(identificación, nombre, apellido);
        
        this.codigo = codigo;
        this.programa = programa;
        this.listadoDeudas = new LinkedList<>();
    }

    //Métodos de acceso
    public int getCodigo() {
        return codigo;
    }

    public Programa getPrograma() {
        return programa;
    }
    
    //Métodos para añadir y eliminar elementos de una lista
    public void añadirDeuda(Deuda deuda) {
        this.listadoDeudas.add(deuda);
    }
    
    public void eliminarDeuda(Deuda deuda) {
        this.listadoDeudas.remove(deuda);
    }
}
