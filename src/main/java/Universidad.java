
import java.util.LinkedList;
import java.util.List;


public class Universidad {
    //Atributos propios
    private int nit;
    private String nombre;
    private String direccion;
    //Relaciones de asociacion uno a uno
    private Periodo periodo;
    //Relaciones de asociacion uno a muchos
    private List<Periodo> listadoPeriodos;
    private List<Asignatura> listadoAsignaturasUniversidad;
    private List<Docente> listadoDocentes;
    private List<Estudiante> listadoEstudiantes;

    //Constructor
    public Universidad(int nit, String nombre, String direccion, Periodo periodo) throws Exception{
        this.nit = nit;
        this.nombre = nombre;
        this.direccion = direccion;
        this.periodo = periodo;
        this.listadoAsignaturasUniversidad = new LinkedList<>();
        this.listadoDocentes =  new LinkedList<>();
        this.listadoEstudiantes = new LinkedList<>();
        this.listadoPeriodos = new LinkedList<>();
    }
    
    //Métodos de acceso
    public int getNit() {
        return nit;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public Periodo getPeriodo() {
        return periodo;
    }
    
    //Métodos para añadir y eliminar elementos de una lista
    //Asignaturas
    public void añadirAsignatura (Asignatura asignatura) {
        this.listadoAsignaturasUniversidad.add(asignatura);
    }
    
    public void eliminarAsignatura (Asignatura asignatura) {
        this.listadoAsignaturasUniversidad.remove(asignatura);
    }
    
    //Docentes
    public void añadirDocente (Docente docente) {
        this.listadoDocentes.add(docente);
    }
    
    public void eliminarDocente (Docente docente) {
        this.listadoDocentes.remove(docente);
    }
    
    //Estudiantes
    public void añadirEstudiante (Estudiante estudiante) {
        this.listadoEstudiantes.add(estudiante);
    }
    
    public void eliminarEstudiante (Estudiante estudiante) {
        this.listadoEstudiantes.remove(estudiante);
    }
    
    //Periodos
    public void añadirPeriodo (Periodo periodo) {
        this.listadoPeriodos.add(periodo);
    }
    
    public void eliminarPeriodo (Periodo periodo) {
        this.listadoPeriodos.remove(periodo);
    }
}
