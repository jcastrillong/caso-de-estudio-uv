
import java.util.LinkedList;
import java.util.List;


public class Periodo {
    //atributos propios
    private String inicia;
    private String fin;
    private short anio;
    //relaciones de asociacion uno a muchos
    private List<Curso> listadoCursos;

    //Constructor
    public Periodo(String inicia, String fin, short anio) throws Exception{
        this.inicia = inicia;
        this.fin = fin;
        this.anio = anio;
        this.listadoCursos = new LinkedList<>();
    }

    //Métodos de acceso
    public String getInicia() {
        return inicia;
    }

    public String getFin() {
        return fin;
    }

    public short getAnio() {
        return anio;
    }

    //Métodos para añadir y eliminar elementos de una lista
    public void añadirCurso(Curso curso) {
        this.listadoCursos.add(curso);
    }
    
    public void eliminarCurso(Curso curso) {
        this.listadoCursos.remove(curso);
    }
    
}
