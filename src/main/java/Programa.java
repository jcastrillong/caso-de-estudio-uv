
import java.util.LinkedList;
import java.util.List;

public class Programa {
    //Atributos propios
    private short codigo;
    private String nombre;
    private String jornada;
    //Relaciones de asociacion uno a muchos
    private List<Semestre> listadoSemestres;

    //Constructor
    public Programa(short codigo, String nombre, String jornada) throws Exception{
        this.codigo = codigo;
        this.nombre = nombre;
        this.jornada = jornada;
        this.listadoSemestres =  new LinkedList<>();
    }

    //Métodos de acceso
    public short getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getJornada() {
        return jornada;
    }
    
    //Métodos para añadir y eliminar elementos de una lista
    public void añadirSemestre(Semestre semestre) {
        this.listadoSemestres.add(semestre);
    }
    
    public void eliminarSemestre(Semestre semestre) {
        this.listadoSemestres.remove(semestre);
    }
}
