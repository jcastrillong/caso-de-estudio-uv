
import java.util.LinkedList;
import java.util.List;

public class Semestre {
    //Atributos propios
    private byte numero;
    //Relaciones de asociacion uno a muchos
    private List<Asignatura>listadoAsignaturasSemestre;

    //Constructor
    public Semestre(byte numero) throws Exception{
        this.numero = numero;
        this.listadoAsignaturasSemestre =  new LinkedList<>(); 
    }
    
    //Métodos de acceso
    public byte getNumero() {
        return numero;
    } 
    
    //Métodos para añadir y eliminar elementos de una lista
    public void añadirAsignatura(Asignatura asignatura) {
        this.listadoAsignaturasSemestre.add(asignatura);
    }
    
    public void eliminarAsignatura(Asignatura asignatura) {
        this.listadoAsignaturasSemestre.remove(asignatura);
    }

    
}
