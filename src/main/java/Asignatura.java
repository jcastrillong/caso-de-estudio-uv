
public class Asignatura {
    
    //Atributos
    private String codigo; 
    private String nombre;
    private byte creditos; //El número de créditos que tiene la asignatura
    private byte intensidad; //Intensidad horaria de la asignatura
    
    //Constructor
    public Asignatura(String codigo, String nombre, byte creditos, byte intensidad) throws IllegalArgumentException {
        //Manejo de Excepciones
        if(codigo == null || "".equals(codigo.trim())) {
            throw new IllegalArgumentException("El código de la asignatura no fue ingresado o es incorrecto");
        }
        if(nombre == null || "".equals(nombre.trim())) {
            throw new IllegalArgumentException("El nombre de la asignatura no fue ingresado o es incorrecto");
        }
        if(creditos <= 0) {
            throw new IllegalArgumentException("EL número de créditos de la asignatura no deben ser menores o iguales a 0");
        }
        if(intensidad <= 0) {
            throw new IllegalArgumentException("La intensidad horaria de la asignatura no debe ser menor o igual a 0");
        }
        
        this.codigo = codigo;
        this.nombre = nombre;
        this.creditos = creditos;
        this.intensidad = intensidad;
    }
    
    //Métodos de acceso
    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public byte getCreditos() {
        return creditos;
    }

    public byte getIntensidad() {
        return intensidad;
    }
    
    //Otros métodos

}

