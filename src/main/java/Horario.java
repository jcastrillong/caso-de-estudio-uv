
//import java.util.Date;
import java.time.LocalTime;

enum DiasSemana {
    LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO
}

public class Horario {
    //Atributos
    //Relaciones de asociación uno a uno
    private LocalTime horaInicio;
    private LocalTime horaFinalizacion;
    //Atributos propios
    private DiasSemana dia;

    //Constructor
    public Horario(LocalTime horaInicio, LocalTime horaFinalizacion, DiasSemana dia) throws Exception{
        this.horaInicio = horaInicio;
        this.horaFinalizacion = horaFinalizacion;
        this.dia = dia;
    }

    //Métodos de acceso
    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public LocalTime getHoraFinalizacion() {
        return horaFinalizacion;
    }

    public DiasSemana getDia() {
        return dia;
    }

}
