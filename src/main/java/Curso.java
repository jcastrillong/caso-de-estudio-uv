
import java.util.LinkedList;
import java.util.List;


public class Curso {
    //Atributos propios
    private short grupo;
    private byte totalCupos;
    //relaciones de asociacion uno a uno
    private Asignatura asignatura;
    private Docente docente;
    //relaciones de asociacion uno a muchos
    private List<Cupo> listadoCupos;
    private List<Horario> listadoHorarios;

    //Constructor
    public Curso(short grupo, byte totalCupos, Asignatura asignatura, Docente docente) throws Exception{
        this.grupo = grupo;
        this.totalCupos = totalCupos;
        this.asignatura = asignatura;
        this.docente = docente;
        this.listadoCupos = new LinkedList<>();
        this.listadoHorarios = new LinkedList<>();
    }

    //Métodos de acceso
    public short getGrupo() {
        return grupo;
    }

    public byte getTotalCupos() {
        return totalCupos;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public Docente getDocente() {
        return docente;
    }
    
    //Métodos para añadir y eliminar elementos de una lista
    //Cupos
    public void añadirCupos(Cupo cupo) {
        this.listadoCupos.add(cupo);
    }
    
    public void eliminarCupos(Cupo cupo) {
        this.listadoCupos.remove(cupo);
    }
    
    //Horario
    public void añadirHorario(Horario horario) {
        this.listadoHorarios.add(horario);
    }
    
    public void eliminarHorario(Horario horario) {
        this.listadoHorarios.remove(horario);
    }
    
    
}
