
import java.util.LinkedList;
import java.util.List;


public class Tabulado {
    //
    //Relaciones de asocioacion uno a uno
    private Periodo periodo;
    //Relaciones de asociacion uno a muchos
    private List<Matricula> listadoMatricula;

    //Constructor
    public Tabulado(Periodo periodo) throws Exception{
        this.periodo = periodo;
        this.listadoMatricula = new LinkedList<>();
    }

    //Métodos de acceso
    public Periodo getPeriodo() {
        return periodo;
    }
    
    //Métodos para añadir y eliminat elementos de una lista
    public void añadirMatricula (Matricula matricula) {
        this.listadoMatricula.add(matricula);
    }
    
    public void eliminarMatricula(Matricula matricula) {
        this.listadoMatricula.remove(matricula);
    }
    
    
}
