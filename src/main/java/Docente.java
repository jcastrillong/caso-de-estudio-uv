
public class Docente extends Persona {
    
    //Atributos
    private String profesion;
    
    //Constructor
    public Docente(String profesion, int identificación, String nombre, String apellido) throws IllegalArgumentException {
        //Hago el llamado al constructor de la super clase,
        //con el objetivo de inicializar los atributos que se heredan
        super(identificación, nombre, apellido);
        
        if(profesion == null || "".equals(profesion.trim())) {
            throw new IllegalArgumentException("La profesión del Docente no fue ingresada");
        }
        this.profesion = profesion;
    }
    
    //Métodos de acceso
    public String getProfesion() {
        return profesion;
    }
    
}
