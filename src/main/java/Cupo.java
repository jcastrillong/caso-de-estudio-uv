
public class Cupo {
    //Atributos propios
    private short cantidad;
    private short disponibles;
    //Relaciones de asociacion uno a uno
    private Programa programa;

    //Constructor
    public Cupo(short cantidad, short disponibles, Programa programa) throws Exception{
        this.cantidad = cantidad;
        this.disponibles = disponibles;
        this.programa = programa;
    }

    //Métodos de acceso
    public short getCantidad() {
        return cantidad;
    }

    public short getDisponibles() {
        return disponibles;
    }

    public Programa getPrograma() {
        return programa;
    }
    
    
    
    
    
}
