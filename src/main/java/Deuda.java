
public class Deuda {
    //Relaciones de asociacion uno a uno
    private Periodo periodo;

    //Constructor
    public Deuda(Periodo periodo) throws Exception{
        this.periodo = periodo;
    }

    //Métodos de acceso
    public Periodo getPeriodo() {
        return periodo;
    }
    
    
}
