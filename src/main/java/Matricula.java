
import java.util.Date;


//Librerias



public class Matricula {
    //Atributos
    //Relaciones de asociación uno a uno(cero a uno)
    private Date horaMatriculada;
    private Date horaCancelada;
    private Curso curso;

    //Constructor
    public Matricula(Date horaMatriculada , Curso curso) throws Exception{
        this.horaMatriculada = horaMatriculada;
        this.horaCancelada = null;
        this.curso = curso;
    }

    //Métodos de acceso
    public Date getHoraMatriculada() {
        return horaMatriculada;
    }

    public Date getHoraCancelada() {
        return horaCancelada;
    }

    public Curso getCurso() {
        return curso;
    }

    
    
}
